---
pagetitle: "Lecture 10"
title: "\nTaller de R: Estadística y programación\n"
subtitle: "\nLectura 10: Regresiones \n"
author: "\nEduard F. Martínez-González\n"
date: "Universidad de los Andes | [ECON-1302](https://github.com/taller-R)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---
```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

## Hoy veremos

### **1.** Prólogo 

### **2.** ¿Usar R? 

### **3.** Configuración inicial 

### **4.** Introducción al entorno R

### **5.** Líberias

### **6.** Directorio de trabajo

### **7.** R + Git


<!------------------------------>
<!-------- -------->         
<!------------------------------>

## Antes de replicar la clase...

Vamos a verificar la versión de R y a llamar las librerías que vamos a usar en la clase

```{r,eval=T,include=T}
R.version.string # Versión de R

# llamar/instalar librería pacman
if(!require(pacman)){install.packages("pacman") ; require(pacman)}  

# llamar y/o instalar las librerías de la clase
p_load(tidyverse, broom, mfx,  margins,
       estimatr, lmtest, fixest,  modelsummary,  stargazer)  
```

<!----------------------------------------------------------------------------->
<!-----------------------------    Prólogo    --------------------------------->
<!----------------------------------------------------------------------------->

# [1.] Regresiones basicas
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Hola


<!----------------------------------------------------------------------------->
<!-----------------------------    Prólogo    --------------------------------->
<!----------------------------------------------------------------------------->

# [2.] Prólogo
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>




<!--------------------->
<!--- Checklist --->
<!--------------------->
# Gracias
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Hoy vimos...

☑ ¿Por qué usar R?

☑ Configurar R, RStudio y Git

☑ Introducción al entorno R

☑ Gestionar líberias

☑ Directorio de trabajo

☑ R + Git

<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.3em ; color:#CC0000 ; font-weight:bolder ; text-align:left}
.reveal section h3 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section h4 {font-size:0.9em ; color:#CC0000 ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section href {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:0.8em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {display: inline-block; border: 0px; background-color: #FFFFFF; align="center"}
</style>

